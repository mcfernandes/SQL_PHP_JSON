package com.vigion.sql;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by maria on 19/01/2016.
 */
public class WebConnection {

    //SendPostRequest()
    //Executa script php online com insert, update ou delete: Método POST
    //Envia dados e o nome do script a executar
    //o server revebe a comunicação com o carimbo POST, extrai os dados e insre-os no script.
    //em variaveis defindas para esse efeito
    //Executa o script (insert, update ou delete)  devolve uma resposta: uma string tbm do script
    //HttpUrlConnection recebe a resposta que, depois de interpretada e convertida, é mostrada.
    //
    //O metodo tem 2 paramentros :
    //  1- URL do script php, para onde são enviadas os dados (POST REQUEST) Pedido de Entrega
    //  2- Collection HashMap (ValuePair) com os dadosa enviar com o PostRequest

    public String sendPostRequest(String phpUrl, HashMap<String, String> postData)
    {

        URL url;                //Objeto URL para formatar o endereço a enviar

        Log.i("LOG:", "WebConnection - URL:" + phpUrl);

        //StringBuilder para tratar a resposta do servidor php
        StringBuilder sb = new StringBuilder();
        try{
            url = new URL(phpUrl);          //Inicializa com o url recebido por parametro

            //Cria uma ligação
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            //Configuração da Ligação
            conn.setConnectTimeout(15000);      //Limite de tempo para fazer a ligação
            conn.setReadTimeout(15000);         //Limite de tempo para receber resposta do server
            conn.setRequestMethod("POST");      //Tipo de ligação : Post - Entrega de dados a um script
            conn.setDoInput(true);              //Sentido de dados que vai ser processado
            conn.setDoOutput(true);             //Neste caso: Saída para Post e Entrada com a resposta


            OutputStream os = conn.getOutputStream();       //Cria um canal de saida

            //Cria um cais de saida para escrita dos dados e liga-o ao canal de saida
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            //Escrita dos dados nos cais de saída
            //É necessario converte-los numa string com o formato url, para simular o Browser
            //convertData2Url() faz a Conversão dos dados da collection para esse formato
            writer.write(convertData2Url(postData));

            writer.flush();     //Comunicação executada
            writer.close();     //Fecho do cais de saida
            os.close();         //Fecho do canal de saida

            //Resposta
            int responseCode = conn.getResponseCode();      //Obtem code com resposta do server PHP
            if (responseCode == HttpsURLConnection.HTTP_OK){        //Se comunica ok

                //Abre cais e canal de entrada para receber resposta Completa do server php
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();                   //Inicialização da StringBuilder
                String response;                            //String para ler linha a linha

                //Leitura, linha a linha, da resposta do server, para uma StringBuilder
                while ((response = br.readLine()) !=null) {
                    sb.append(response);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            Log.i("LOG:", "WebConnection - sendPostRequest - Exceção: " + ex.getMessage());
        }
        return sb.toString();       //Devolve a StringBuilder com a resposta do server.
    }

    //-------------------------------------------------------------------------------------------------
    //  sendGetRequest() tem 2 variantes:
    //  - 1 parametro, obtem queries de todos os registos: 1ºparametro é para o script php
    //  - 2 parametro, obtem queries de todos os registos: 2º é para o id
    //-------------------------------------------------------------------------------------------------

    //sendGetRequest(1 parametro)
    //Executa script php online com query para obter registos da BD: Método GET
    //Envia Dados e o nome do script a esecutar, com o carimbo GET.
    //O server recebe a comunicaçã, extrai os dados e insere-os no script, em variaveis proprias
    //Executa o script com a query, agora com os dados passados e devolve o resultado da query ou erro.
    //HttpUrlConection recebe a resposta, com os registos, através de con.getInputStream
    //  - pelo canal de entrada inPutStreamReader
    //  - nos cais de entrada BufferedReader
    //os dados são empilhados na StringBuilder sb e devolvida para quem chamou o método
    //
    //Os métodos tem 1 parametro : URL do scriptphp;

    public String sendGetRequest(String phpURL){

        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(phpURL);      //Converte a string com url do script php
            HttpURLConnection con  = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversão e resposta do server, com os dados, para uma StringBuilder
            //lembrar que vem numa string, com formato de um array json
            String strLine;
            while((strLine = bufferedReader.readLine()) !=null){
                sb.append(strLine + "\n");      //Leitura, linha a linha, da resposta do server
            }
        }
        catch (Exception ex){
            Log.i("LOG:", "WebConnection - sendGetRequest1 - Exceção: " + ex.getMessage());
        }
        return  sb.toString();      //Devolve a StringBuilder com os dados
    }

    //sendGetRequest(2 parametros)
    //Executa script php online com query para obter um só registo, identificado por ID: Método GET
    //Envia dados e o nome do script a executar.
    //O server recebe a comunicação como carimbo GET, extrai os dados e insere-os nos scripts,
    //em variaveis definidas para esse efeito
    //Executa o script com a query, agora com dados reais e devolve o resultado da query ou erro.
    //HttpUrlConnection recebe a resposta, com os registos, através de con.getInputStream
    //  - pelo canal de entrada inPutStreamReader
    //  - no cais de entrada BufferedReader
    //os dados são empilhados na StringBuilder sb e devolvida para quem chamou o metodo
    //
    //O metodo tem 2 paramentros : URL do script php e o nProc do registo a obter;

    public String sendGetRequest(String phpURL, String nProc){

        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(phpURL+nProc);      //Converte a string com url do script php
            HttpURLConnection con  = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversão e resposta do server, com os dados, para uma StringBuilder
            //lembrar que vem numa string, com formato de um array json
            String strLine;
            while((strLine = bufferedReader.readLine()) !=null){
                sb.append(strLine + "\n");      //Leitura, linha a linha, da resposta do server
            }
        }
        catch (Exception ex){
            Log.i("LOG:", "WebConnection - sendGetRequest1 - Exceção: " + ex.getMessage());
        }
        return  sb.toString();      //Devolve a StringBuilder com os dados
    }

    //Metodo auxiliar para conversão dos dados a enviar pelo sendPostRequest()
    //Converte os dados da Collection ValuePair recebida, numa string url, como nos browsers
    private String convertData2Url(HashMap<String, String> dados) throws UnsupportedEncodingException{
        StringBuilder dadosConvertidos = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : dados.entrySet()){    //Foreach: para cara par
            if (first)
                first = false;
            else
                dadosConvertidos.append("&");       //A partir do 1º concatena-os com &

            //Converte numa string URL, em que os dados são pares ligados por "=" e separados por &
            dadosConvertidos.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            dadosConvertidos.append("=");
            dadosConvertidos.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return dadosConvertidos.toString();
    }
}