package com.vigion.sql;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //views
    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    private Button buttonAdd;
    private Button buttonLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Aquisição do controlo dos objetos gráficos
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

       // buttonAdd = (Button)findViewById(R.id.buttonAdd);
        //buttonLista = (Button)findViewById(R.id.buttonLista);

        //Botões e Listeners
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAluno();
            }
        });

        buttonLista = (Button)findViewById(R.id.buttonLista);
        buttonLista.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //startActivity(new Intent(MainActivity.this, AlunoListView.class));
            }
        });

        //Botoes FAB e Listeners
        FloatingActionButton fabAlunoAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fabAlunoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Adicionar um aluno", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //Adiciona novo registo numa classe assíncrona
                addAluno();
            }
        });

        FloatingActionButton fabAlunoList = (FloatingActionButton) findViewById(R.id.fabAlunoList);
        fabAlunoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //Chama a ativity com a listView de alunos
                //startActivity(new Intent(MainActivity.this, AlunoListView.class));

            }
        });
        Log.i("LOG:", "MainActivity - onCreate() COMPLETA");
    }

    //Adiciona um aluno
    private void addAluno(){

        //Recolha dos dados, inseridos pelo user
        final String nome = editTextNome.getText().toString().trim();
        final String username = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

      //  if (nome.isEmpty() || username.isEmpty() || password.isEmpty()){
      //      Toast.makeText(MainActivity.this, "Campos sem dados", Toast.LENGTH_LONG.show());
      //  }

        class AddAluno extends AsyncTask<Void, Void, String>{

            ProgressDialog loading;     //ProgressBar em caixa de mensagem

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this,
                        "A processar envio de dados...",
                        "Só um momento...", false, false);
            }

            @Override
            protected String doInBackground(Void... v) {
                //Cria uma collection value pair, de 2 strings: nomes de campos e os dados das EditText
                HashMap<String, String> dados = new HashMap<>();
                dados.put(Config.KEY_NOME, nome);
                dados.put(Config.KEY_USERNAME, username);
                dados.put(Config.KEY_PASSWORD, password);

                //Comunicação dos dados para Insert, usando o método sendPostRequest() da classe WebConnection
                WebConnection wc = new WebConnection();         //Cria objeto WebConection
                Log.i("LOG:", "MainAtivity - Class Main Ativity - doInBackground - vou chamar WebConnection.sendPostRequest() com url:" + Config.URL_ADD);
                String resp = wc.sendPostRequest(Config.URL_ADD, dados);            //Envia dados e espera resposta
                Log.i("LOG:", "MainAtivity - Class AddAluno - doInBackground - vou chamar WebConnection.sendPostRequest() com string:" +resp);
                return resp;        //devolve a resposta
            }


            @Override
            protected void onPostExecute(String response){
                super.onPostExecute(response);
                loading.dismiss();
                Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG).show();
               //startActivity(new Intent(AddAluno.this, AlunoListView.class));
            }
        }

        AddAluno newAluno = new AddAluno();     //Cria um novo objeto assincrono
        newAluno.execute();                     //Executa a thread.
    }

    //-----------------------------------------------------------------------------------------------------------
    //MENUS
    //--------------------------------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
