package com.vigion.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;

public class AlunoListView extends AppCompatActivity {

    private ListView listView;  //Obtem java para controlo do objeto grafico ListView
    private String JSON_STRING; //String para tratamento de dados JSON

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabAlunoAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fabAlunoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Aquisição do controlo da ListView e construção do Listener
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View View, int position, long id) {
                //Obtêm o item com as duas string: o 1º é nProc, o 2º é name
                HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

                //Extrai o nProc do item
                String itemNproc = map.get(Config.TAG_NPROC).toString();

                //Prepara a chamada da activity para exibir os dados do item clicado
                //TODO: Por fazer
            }
        });

        getAlunos();        //Método com classe assincrona para obter a lista d alunos da BD

        FloatingActionButton forAlunoAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        forAlunoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AlunoListView.this, MainActivity.class));

            }
        });
    }

}