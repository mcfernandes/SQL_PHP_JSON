package com.vigion.sql;

/**
 * Created by maria on 19/01/2016.
 */

public class Config {

    //Localização do server dos scripts PHP
    //public static final String HOST_PHP_LOCATION = "http://x12345.net23.net/CRUD_ESCOLA/";
    public static final String HOST_PHP_LOCATION = "http://10.1.17.128/AndroidSQL/";

    //Endereço dos scripts php u
    // envia informação para o server
    public static final String URL_ADD = HOST_PHP_LOCATION + "addAluno.php";
    public static final String URL_GET_ALL = HOST_PHP_LOCATION + "getAllAluno.php";
    public static final String URL_GET_ALUNO = HOST_PHP_LOCATION + "getAluno.php?nProc=";
    public static final String URL_UPDATE_ALUNO = HOST_PHP_LOCATION + "updateAluno.php";
    public static final String URL_DELETE_ALUNO = HOST_PHP_LOCATION + "deleteAluno.php?nProc=";

    //Nomes dos atributos a enviar nos Requests, para os scripts php
    public static final String KEY_NPROC = "nProc";
    public static final String KEY_NOME = "Nome";
    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_PASSWORD = "Password";

    //JSON Tags, a extrair dos dados remetidos pelo server
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_NPROC = "nProc";
    public static final String TAG_NOME = "Nome";
    public static final String TAG_USERNAME = "UserName";
    public static final String TAG_PASSWORD = "Password";

    //Identificador do registo a passar no Intent entre as activities da listView e AlunoEdit
    public static final String ALUNO_NPROC = "nProc";
}